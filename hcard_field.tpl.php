<?php
  /*
    field array
    $$variables


    vCard available values
      $prefix
      $first_name
      $last_name
      $suffix
      $full_name
      $title
      $organization
      $address_type
      $address
      $city
      $region
      $country
      $phone_default
      $phone_cell
      $phone_fax
      $phone_home
      $email
      $link // contact URL


    Vcard link info
      $label - Link label user data
      $vcard_url  - URL for custom formatting
      $vcard_link  - full link to get vcard
  */
?>
<div class="vcardfield-wrapper <?php print $classes . ' ' . $zebra; ?>">

    <?php print $full_name; ?><br />
    <?php print $vcard_link; ?>

</div>
